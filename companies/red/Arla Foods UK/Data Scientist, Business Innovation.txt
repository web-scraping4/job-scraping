Data Scientist, Business Innovation

Brinner du för att jobba med maskininlärning och prediktiv programvara?
Intresserad av att tillämpa dessa tekniker med en stor inverkan direkt på verksamheten och kunder?

Vi på Arla Foods SE söker nu en Data Scientist till Stockholm/Solna.

Du blir en del i en funktion som utvecklar övergripande processer och system samt identifierar nya utvecklingsområden med fokus på kundnära digitala tjänster. Som exempel kan detta röra sig om tjänster som supporterar kund med information och/eller köp av våra produkter eller leveranser.

Vad innebär rollen?
Du kommer vara en del av vår funktion som arbetar med Big Data, BI samt AI med fokus på analys och prediktiv analys. Inom ramen för rollen ligger också ett ansvar att identifiera och implementera ny teknisk funktionalitet för vidareutveckling av våra befintliga digitala tjänster.

Exempel på uppgifter som ingår i ditt dagliga arbete;
Samverka med intressenter såsom kollegor, kunder och leverantörer för att förstå affärsproblem
Analysera data för att identifiera nya utvecklingsområden och lösningar
Designa och utveckla utforskande modeller med maskininlärning och optimeringstekniker
Produktifiera dessa lösningar
Vem är du?
Vi söker en driven person som vill starta sin yrkeskarriär på Arla, vi ser gärna att du är nyutexaminerad från högskolan/universitetet eller har en likvärdig bakgrund. Du har goda programmeringskunskaper och gillar att analysera stora datamängder.

Vi lägger stor vikt vid personliga egenskap och tror att du är metodisk och strukturerad som person och har lätt för att beskriva tekniskt komplicerade saker på ett pedagogiskt sätt i dialogen med andra. Rollen kräver utöver detta en förmåga att kunna arbeta självständigt och tillsammans med andra.

Vi ser att du har följande förmågor :
Identifiera användbara lösningar genom dataanalys
Designa och utveckla utforskande modeller med maskininlärning och optimeringstekniker
Produktifiera/realisera dessa lösningar
Vi ser att du har följande kompetenser :
Analytisk förmåga att sätta sig in i och förstå affärs- och verksamhetsnära problem
Erfarenhet av verktygslåda för maskin/djupinlärning
Erfarenhet av programmering, Python och R
Pedagogisk och kommunikativ
Flytande språkkunskaper i engelska och svenska
Det är meriterande om du även har:
Erfarenhet av att arbeta med presentationsverktyg såcom Power BI, Dundas Bi eller Tableau
Agil utvecklingsmetodik
Förstå slutanvändarens och kundernas behov
Vad erbjuder vi?
Detta är en fantastisk chans att komma in i ett stort globalt företag som Arla. Hos oss erbjuds du tillgång till resurser, stöd och verktyg som hjälper dig i ditt uppdrag.

Genom vår positiva dynamik, atmosfär och storlek kan vi erbjuda dig en inspirerande roll och arbetsplats där du får möjlighet att både utveckla och utvecklas. Vi söker dig som vill vara med på vår resa, som vill skapa framtidens dagligvaruhandel och som vill växa med oss.

Ansökan och kontakt
Sök nu! Vi gör ett löpande urval och tjänsten kommer att tillsättas så fort vi hittat rätt person.
Välkommen att kontakta Thomas Petersson, Manager Process & systemutveckling på +4670 698 28 32 för mer information om tjänsten.

COVID-19 Notice:

We are busy producing dairy products so we can continue #fillingtheshelves and #feedingthenation. Recruiting talent to Arla thus stays one of our top priorities also during these difficult times. Necessity is the mother of invention, so we have moved to virtual hiring and onboarding processes so we enable candidates and our recruitment teams to get close while respecting social distancing. If at the final stages of a recruitment, a candidate would prefer to meet in person, we take all the necessary precautions to ensure everyone's safety.

#LI-HF

LinkedIn#LI-DNP

Ref n. 67405