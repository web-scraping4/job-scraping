Who we are


Fueled by a fundamental belief that having access to financial services creates opportunity, PayPal (NASDAQ: PYPL) is committed to democratizing financial services and empowering people and businesses to join and thrive in the global economy. Our open digital payments platform gives PayPal’s 305 million active account holders the confidence to connect and transact in new and powerful ways, whether they are online, on a mobile device, in an app, or in person. Through a combination of technological innovation and strategic partnerships, PayPal creates better ways to manage and move money, and offers choice and flexibility when sending payments, paying or getting paid. Available in more than 200 markets around the world, the PayPal platform, including Braintree, Venmo and Xoom, enables consumers and merchants to receive money in more than 100 currencies, withdraw funds in 56 currencies and hold balances in their PayPal accounts in 25 currencies.

When applying for a job you are required to create an account, if you have already created an account - click Sign In.

Creating an account will allow you to follow the progress of your applications. Our system does have some requirements that will help us process your application, below are some guidelines for creation of your account:
Provide full legal First Name/Family Name – this is important for us to ensure our future hires have the right system set up.
Please Capitalize first letter of your First and Last Name.
Please avoid using fully capitalized text for your First and/or Last Name.
NOTE: If your name is hyphenated or has multiple capitalization, please use the same format as your government ID.
Job Description Summary:

In 2018, iZettle became part of the PayPal family. iZettle is on a mission to help small businesses succeed in a world of giants. Tens of millions of small businesses worldwide are still being underserved by the traditional players. By giving small businesses the tools to get paid, sell smarter and grow, we’re empowering them to reach their full potential. Together with PayPal, we can reach 24 million merchants across the world with more of the affordable tools they need to thrive. Join us in putting an end to the ordinary way the world defines financial freedom.

Job Description:

Do you want to use data to help small businesses succeed?

We’re looking for a curious and engaged Senior Data Analyst to join our Business Analytics team, focusing on central operations. Are you passionate about helping small businesses? Do you believe in the power of data to make decisions? Do you enjoy solving business problems and seeing a measurable impact of your work? Then join our team in Stockholm!

Make sense of big data for small businesses

Business Analytics is part of the Analytics and Data Science team at iZettle. The team is responsible for supporting commercial teams such as Marketing, Sales and Operations in making informed decisions based on data.

Working with central operations, you will be at the very heart of the company and help our leaders in making decisions. How is iZettle performing? Where are the opportunities and what should we focus on? How do we set the right KPIs and targets to deliver on our mission?

With the power of Google Cloud Platform and using state-of-the-art tools and techniques, we make sense of data to continuously improve our business and create value for our merchants.

As a Senior Data Analyst for central operations
You’ll support leadership in understanding the business’s performance
You’ll translate company goals into operational targets for the teams
You’ll partner with department leads and other stakeholders to enable and promote data-driven decision making and provide clarity on how each team is contributing to iZettle’s success
You’ll own end-to-end analytics for your area, from tracking and defining KPIs to setting targets and evaluating the impact of new initiatives
You’ll empower other iZettlers to use data in their work by training them and developing our data platform and making data accessible through our BI tools
You’ll assist in coaching and growing others in the team
You’ll articulate and communicate actionable insights and recommendations based on data
The skills you need to succeed

As a Data Analyst at iZettle, you’ll join a team of 15+ inspiring data scientists and analysts who support various areas of iZettle. This is a great opportunity for development, being at the heart of iZettle, driving understanding of our business and seeing your recommendations make an impact. The team works across a wide range of business areas and initiatives, so your role will vary and evolve based on your interest, expertise and business needs.

We’d be thrilled to welcome you to the team if you believe that…
You have a strong business sense and an analytical mindset, with a creative approach to solving problems and always striving to make an impact
You have an understanding of how data can shape business and product decisions
You are a good communicator and you're comfortable explaining different methods and results to an audience ranging from other analysts to c-level executives
You are comfortable with handling uncertainty and being in a fast changing environment
You have previous experience in budgeting and setting operational targets
You have knowledge in statistics and know how to deal with stochastic data
You have strong SQL and programming skills
Additionally, if you have advanced coding skills and/or previous exposure to senior leadership, let us know. If not, we'll be happy to help you get started.

Small minds never helped small businesses.

It takes originality and an open mind to start a small business. That’s why small minds have no place in ours. We’re proud to be an equal opportunity employer and together we uphold PayPal’s One Team Behavior, striving to be a place where everyone can benefit from equal access to professional development and bring their whole self to work.

Subsidiary:

iZettle

Travel Percent:

0

Primary Location:

Stockholm, Stockholm County, Sweden

Additional Locations:

We're a purpose-driven company whose beliefs are the foundation for how we conduct business every day. We hold ourselves to our One Team Behaviors which demand that we hold the highest ethical standards, to empower an open and diverse workplace, and strive to treat everyone who is touched by our business with dignity and respect. Our employees challenge the status quo, ask questions, and find solutions. We want to break down barriers to financial empowerment. Join us as we change the way the world defines financial freedom.

PayPal provides equal employment opportunity (EEO) to all persons regardless of age, color, national origin, citizenship status, physical or mental disability, race, religion, creed, gender, sex, pregnancy, sexual orientation, gender identity and/or expression, genetic information, marital status, status with regard to public assistance, veteran status, or any other characteristic protected by federal, state or local law. In addition, PayPal will provide reasonable accommodations for qualified individuals with disabilities. If you are unable to submit an application because of incompatible assistive technology or a disability, please contact us at paypalglobaltalentacquisition@paypal.com.