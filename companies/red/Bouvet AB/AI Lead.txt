Som konsult och medarbetare på Bouvet kommer du att utmanas och få utmana i komplexa och unika uppdrag där problemställningarna förutsätter en kombination erfarenhet, kreativitet och nytänkande.

Vi investerar i våra medarbetare genom kontinuerlig kompetensutveckling och kunskapsöverföring genom regelbundna ”Bouvetfredagar” där företagets kompetensgrupper träffas men också bjuder in externa föreläsare.

Som anställd i Bouvet får du möjlighet att avsätta arbetstid för att identifiera och utveckla egna projekt. Vi erbjuder marknadsmässiga förmåner och fast lön samt även delägarskap genom aktieprogram.

Om Rollen

Vi söker dig som har praktisk erfarenhet inom AI i kombination med önskan att bygga upp någonting nytt.
Vi arbetar inom AI idag men vill göra det till någonting större. Under 2018 fokuserade Bouvet Norge på AI och är nu ca 30 personer med roller som Data Analysts, Data Scientists och Data Engineers. Vi behöver nu dig som vill göra samma sak för Bouvet Sverige, fast på ditt eget sätt med stöd från dina nya kollegor i Sverige och Norge.

Troligtvis känner du igen dig i någon av rollerna Data Analyst, Data Scientist eller Data Engineer och vill ha möjlighet att jobba med några av Norges bästa inom Data Science, NLP och Reinforcement Learning.

Exakt vad rollen innebär beror mycket på vem du är som person.

Din profil
Minst 4 års praktisk erfarenhet inom AI
God ledarskaps- och presentationskompetens
Initiativförmåga och drivkraft
Meriterande
Goda kunskaper i svenska
Programmeringskunskaper (t ex C#, C++, Java, Scala, SQL, R, Python)
Erfarenhet av Data Enginering (t ex Spark, Hadoop, Kakfa, SQL)
Erfarenhet av att leda förändringsarbete inom data science/analytics/strategy
Konsulterfarenhet
Erfarenhet av rekrytering
Kunskap och erfarenhet av AWS, Azure, Cassandra, Cosmos DB, dashDB, DataBricks, Google Cloud Platform, GPU, HBase, Hive, IoT, Hortenworks, Keras, MapReduce, Maskinlæring, MongoDB, MXNet, MySQL, NoSQL, OracleDB, Pig, PowerBI, Scikit-Learn, TensorFlow, TPU, Watson
Relevant utbildning från universitet/högskola
Din ansökan

Skicka ett mail eller ring mig, så tar vi det därifrån.
Kontor: Stockholm
Jesper Gradin

Avdelningsledare Systemutveckling

jesper.gradin@bouvet.se

0709 43 14 53