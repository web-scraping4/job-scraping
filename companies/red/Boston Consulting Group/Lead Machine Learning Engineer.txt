Location:
Barcelona, Berlin, Frankfurt, London, Madrid, Munich, Oslo, Paris, Stockholm, Warsaw, Zurich

Geography:
Europe & The Middle East

Capabilities:
Big data & advanced analytics, Innovation & product development, Technology & digital

Industries:
Automotive & Mobility, Biopharmaceuticals, Consumer products, Education, Energy & environment, Engineered products & infrastructure, Financial institutions, Health care payers & providers, Insurance, Media & entertainment, Medical devices & technology, Metals & mining, Private equity and principal investment, Process industries & building materials, Public sector, Retail, Social sector, Technology industries, Telecommunications, Transportation, travel & tourism

Who We Are


Boston Consulting Group partners with leaders in business and society to tackle their most important challenges and capture their greatest opportunities. BCG was the pioneer in business strategy when it was founded in 1963. Today, we help clients with total transformation-inspiring complex change, enabling organizations to grow, building competitive advantage, and driving bottom-line impact.

To succeed, organizations must blend digital and human capabilities. Our diverse, global teams bring deep industry and functional expertise and a range of perspectives to spark change. BCG delivers solutions through leading-edge management consulting along with technology and design, corporate and digital ventures—and business purpose. We work in a uniquely collaborative model across the firm and throughout all levels of the client organization, generating results that allow our clients to thrive.

Practice Area Profile

BCG GAMMA combines innovative skills in computer science, artificial intelligence, statistics, and machine learning with deep industry expertise. The BCG GAMMA team is comprised of world-class data scientists and business consultants who specialize in the use of advanced analytics to get breakthrough business results. Our teams own the full analytics value-chain end to end: framing new business challenges, building fact-bases, designing innovative algorithms, creating scale through designing tools and apps, and training colleagues and clients in new solutions. Here at BCG GAMMA, you’ll have the chance to work with clients in every BCG region and every industry area. We are also a core member of a rapidly growing analytics enterprise at BCG - a constellation of teams focused on driving practical results for BCG clients by applying leading edge analytics approaches, data, and technology.

What You'll Do

POSITION SUMMARY:


BCG Gamma is seeking a Lead Machine Learning Engineer to join our engineering team. The ideal candidate will have industry experience working on a range of different machine learning disciplines, e.g. anomaly detection, payment fraud, fraud detection, search ranking, text/sentiment classification, spam detection and others. The position will involve taking these skills and applying them to some of the most exciting and massive data and analytics problems across multiple industries. We are looking for machine learning engineers to join the ML effort for our teams in our European hubs, building ML-based systems, tools, and services that serve as infrastructure for our internal and external clients.

As a strong software leader and an expert in building complex systems, you will be responsible for inventing how we use technology, machine learning, and data to enable the productivity of our clients. You will help envision, build, deploy and develop our next generation of data engines and tools which will help our clients fundamentally transform their business.

You will be bridging the gap between business and engineering, and functioning with deep expertise in both worlds.

RESPONSIBILITIES:


We are looking for a Lead Machine Learning Engineer who can bring bleeding edge machine learning models into production together with a highly multi-disciplinary team of scientist, engineers, partners, product managers and subject domain experts.

What You'll Bring (Experience & Qualifications)

REQUIREMENTS:


• Masters degree or PhD in computer science (or in a highly related area)

• 8+ years of experience in algorithms, data structures, and object-oriented programming

• Previous consulting experience and/or strong leadership positions

• Good understanding of machine learning fundamentals and deep learning

• Implementation experience in machine learning algorithms and applications

• Management/team lead experience, experience working with global and remote teams

• Experience guiding teams in addressing the technical and delivery challenges

• Excellent communication skills

TECHNOLOGIES:

• Strong programming skills in at least one object oriented programming language (Java, Scala, C++, Python, etc.)

• Strong skills in parallel processing technologies and languages: Hadoop, Spark, Scala etc.

• Experience with Python applied to machine learning (Pandas, Scikit-learn, Scipy, Numpy etc.)

• Strong knowledge of machine learning techniques required (KNN, random forest, Bayesian statistics etc.)

• Strong knowledge of machine learning techniques preferred (TensorFlow, Keras, PyTorch,Caffe, MxNet)

• Cloud: AWS/GCP/Azure

• DevOps: Kubernetes, Docker, CI/CD

• Building REST API

• Knowledge of relational database management: SQL, PostgreSQL, Microsoft SQL etc.

• Knowledge of NoSQL databases (MongoDB, Neo4j, Redis etc.)

• Unix-based command line & development tools

WORK ENVIRONMENT:


• Fast-paced, intellectually intense, service-oriented environment

• Position is located in Gamma European hubs (Paris, London, Germany, Nordics etc.)

• Fluency in local language and English are required

• Expect time spent traveling

Date Posted:
01-Jun-2019

Boston Consulting Group is an Equal Opportunity Employer. All qualified applicants will receive consideration for employment without regard to race, color, age, religion, sex, sexual orientation, gender identity / expression, national origin, disability, protected veteran status, or any other characteristic protected under national, provincial, or local law, where applicable, and those with criminal histories will be considered in a manner consistent with applicable state and local laws.
BCG is an E-Verify Employer. Click here for more information on E-Verify.