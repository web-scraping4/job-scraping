Location:
Berlin, Copenhagen, Frankfurt, Helsinki, London, Munich, Oslo, Paris, Sao Paulo, Stockholm, Warsaw

Geography:
Central & South America, Europe & The Middle East

Capabilities:
Big data & advanced analytics, Innovation & product development, Pricing, Strategy, Technology & digital

Industries:
Automotive & Mobility, Biopharmaceuticals, Consumer products, Education, Energy & environment, Engineered products & infrastructure, Financial institutions, Health care payers & providers, Insurance, Media & entertainment, Medical devices & technology, Metals & mining, Private equity and principal investment, Process industries & building materials, Public sector, Retail, Social sector, Technology industries, Telecommunications

Who We Are


Boston Consulting Group partners with leaders in business and society to tackle their most important challenges and capture their greatest opportunities. BCG was the pioneer in business strategy when it was founded in 1963. Today, we help clients with total transformation-inspiring complex change, enabling organizations to grow, building competitive advantage, and driving bottom-line impact.

To succeed, organizations must blend digital and human capabilities. Our diverse, global teams bring deep industry and functional expertise and a range of perspectives to spark change. BCG delivers solutions through leading-edge management consulting along with technology and design, corporate and digital ventures—and business purpose. We work in a uniquely collaborative model across the firm and throughout all levels of the client organization, generating results that allow our clients to thrive.

Practice Area Profile

BCG GAMMA combines innovative skills in computer science, artificial intelligence, statistics, and machine learning with deep industry expertise. The BCG GAMMA team is comprised of world-class data scientists and business consultants who specialize in the use of advanced analytics to get breakthrough business results. Our teams own the full analytics value-chain end to end: framing new business challenges, building fact-bases, designing innovative algorithms, creating scale through designing tools and apps, and training colleagues and clients in new solutions. Here at BCG GAMMA, you’ll have the chance to work with clients in every BCG region and every industry area. We are also a core member of a rapidly growing analytics enterprise at BCG - a constellation of teams focused on driving practical results for BCG clients by applying leading edge analytics approaches, data, and technology.

What You'll Do

POSITION SUMMARY:


As a Senior Software Engineer, you will be responsible for advancing software solutions to support BCG Gamma’s analytics platform and clients. Your strong analytical skills and ability to develop innovative problem solving solutions will support mission critical decision analytics for our clients. Additional responsibilities will include developing feature enhancements to our platform, developing industry-leading analytics software solutions and methodologies and provide talks and papers in industry leading conferences on behalf of BCG Gamma. We are looking for talented individuals with a serious commitment to software development, data science, large data analytics and transforming organizations into analytics led innovative companies.

RESPONSIBILITIES:


• Apply software development practices and standards to develop robust and maintainable software

• Develop abstract analytic models to solve complex problems for decision analysis

• Maintain an active role in every part of the software development life cycle

• Help mentor and guide junior developers

• Guide non-technical teams and consultants in understanding analytics at scale, infrastructure as code and best practices for robust software development

• Optimize and enhance computational efficiency of algorithms and software design

• Design data structures and visualization of results to provide users actionable intelligence and situational awareness of supporting data

• Interact directly with clients on new features for future product releases

• Provide thought leadership to software design and solutions

What You'll Bring (Experience & Qualifications)

REQUIREMENTS:


• Masters Degree or PhD in Computer Science, Statistics, Operations Research, or related field

• 5 to 8 years of experience in a software development environment

• Proficiency with analytic software programming

• Experience with Hashicorp Vault, Terraform & Consul

• Exposure to analytics applications or experience building analytics tools

• Familiarity with data science methods and scaling data science methods

• Proficiency with infrastructure as code principles

• Familiarity with the storage, manipulation and management of relational, non-relational and streaming data structures

• Analytic reasoning and complex problem solving involving mathematical programming and big data problems

• Understanding of parallel computing

TECHNOLOGIES:


• Languages programming: Python, GO, C++, Java, Scala, JavaScript, TypeScript

• DevOps: Docker, Kubernetes, CI/CD, Terraform, unix-based command line

• Full stack development: GraphQL, React

• Data: SQL, Spark, Hadoop

• Data Science and machine learning (Pandas, Scikit learn)

• Deep learning (Tensorflow, Keras etc.)

• Cloud: AWS/Azure/Google

WORK ENVIRONMENT:


• Fast-paced, intellectually intense, service-oriented environment

• Position is located in Gamma European hubs (Paris, London, Germany, Nordics etc.)

• Fluency in local language and English are required

• Expect time spent traveling

Date Posted:
05-Jul-2018

Boston Consulting Group is an Equal Opportunity Employer. All qualified applicants will receive consideration for employment without regard to race, color, age, religion, sex, sexual orientation, gender identity / expression, national origin, disability, protected veteran status, or any other characteristic protected under national, provincial, or local law, where applicable, and those with criminal histories will be considered in a manner consistent with applicable state and local laws.
BCG is an E-Verify Employer. Click here for more information on E-Verify.