About Qapital

Qapital is a new kind of banking experience that empowers people to maximize their happiness by saving, spending, budgeting, and investing with their goals in mind. With 1.9 million members, Qapital blends behavioral economics with technology, providing people with the tools they need to make managing money easy and fun.

Long-recognized as a fintech pioneer, Qapital now has more than 70,000 5-star app ratings. Apple awarded us App of the Day in March 2020, and in 2019, CNET named us on their list of “Best money-saving apps to help stay within a budget.” Fast Company listed us as one of “The 25 Best New Apps of 2018”, and the year before, Google named us “Most Innovative App of the Year.”

We are a team of designers, behavioral scientists and financial technologists on a mission to build a new banking experience from scratch. Join us, and be part of a team that is changing the way people think about everyday banking.

What We’re Looking For

Qapital Product Managers work with teams of engineers and designers to build products. We are looking for extremely entrepreneurial Product Owners to help innovate and execute product initiatives across the company. Qapital is a mature startup and this is a fantastic opportunity for the right person to take charge and shape our product strategy and execution as we build something great.

Responsibilities
Drive product development with a team of world-class engineers and designers
Understand Qapital’s strategic and competitive position and deliver products that are best in the industry
Deeply understand the Qapital mission and contribute to defining, articulating, and communicating our product strategy in order to ensure that quality product are being delivered to our customers
Lead the product team through ideation, technical development, and launch of innovative products
Integrate usability studies, research and market analysis into product requirements to enhance user satisfaction
Define and analyze metrics that inform the success of products
Utilize data science and analysis to make initiated product decisions
Maximize efficiency in a constantly evolving environment where the process is fluid and creative solutions are the norm
Create and manage the product roadmap that drive relevant KPIs
Set up a learnings/feedback cycle to integrate results from experimentation into the product development process
Clearly communicate product plans, benefits and results to the team as well as all employees at Qapital
Requirements
At least 7-10 years experience with product management and agile methodologies
A deep passion for mobile products and services
Proven leadership experience with great communication and presentation skills
Interdisciplinary thinking, with the ability to thrive in a cross-functional role
Outstanding organizational and interpersonal skills
Can-do attitude and a willingness to go the extra mile to win
Excellent analytical skills with the ability to prioritize
Great project management skills and a passion for the fast-paced startup life
Experience from Financial Services or Financial Technology is an advantage