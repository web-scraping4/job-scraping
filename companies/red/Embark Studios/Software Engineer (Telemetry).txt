At Embark, we want to empower everyone to create games.

To realize this dream of ours, we explore and apply the latest technology, and we reimagine what games can become and how they should be built.

All of us at Embark take part in shaping our culture. We succeed together, we fail together, and we learn together.

If you’re passionate about games and our mission, bring your expertise, your enthusiasm, your crazy ideas, and let’s change the games industry forever.

At Embark we offer competitive salaries, a generous profit-sharing program, and a journey into the unknown, to build creative, surprising and beautiful experiences together. Our newly renovated office is centrally located in Stockholm.

Overview
As our Telemetry Engineer at Embark you are a key person to ensure that the Data Science team has data to understand and improve the high-quality, polished experiences we want to provide to our players. Today the team you will join consists of a Senior Data Scientist and a Senior Data Engineer and in close collaboration with them you will define and implement telemetry events in the game. We think you have a background as a Software Engineer and have an interest in data. The game we're making is built in Unreal, using C++, blueprints and a scripting language called Angelscript.

Example of responsibilities are
Implement telemetry events and develop the framework for sending telemetry within the game code base using C++ and Rust
Work closely with the Game Designers and the Data Science team to design and create telemetry specifications
Build tools to make the data more available to our data engineers and data scientists
Create and automate test systems to reduce manual quality testing
It would be awesome if you have
A creative and curious mind
Love for data
Interest in or experience from working with game engines, e.g. Unreal or Unity
Strong Experience with C++, Rust or similar languages
Basic knowledge of SQL
A good grasp of performance, optimization and data structures
The ability to be a great team player with good communication skills
Professional English communication skills.
Experience from game development is meritorious
We welcome people from all backgrounds and are looking forward to reading more about you (in English)!