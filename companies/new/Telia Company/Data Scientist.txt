Right now, we are looking for a data scientist to join our team. We are spearheading Telia Company’s journey of becoming a truly data-driven organization by leveraging big data and advanced analytics.

I am Camilla Sternberg and I am leading our Analytical products unit. Our mission is to work closely with our internal stakeholders to enable new business opportunities as well as to optimize investments and operations based on big data insights. We are a bunch of great people and I am super proud of every member of my team.

Your next challenge?

If you want to be at the forefront of big data and advanced analytics, to help deliver the best possible experiences to millions of users on our networks every day and to work with one of the greatest data lakes in Scandinavia, you are a perfect fit for my team.

As a Data Scientist, you will play a vital role in a unique and inspiring environment. You will be part of a highly skilled and passionate team, focused on leveraging our vast data to create business value for different lines-of-business across six different countries. You will bring data to life by applying an agile mind-set and by using state-of-the-art analytic tools, models & methods.

We are already eleven highly driven and curious colleagues with different expertise areas, such as, business analysis, data visualization, statistics and machine-learning. We work in cross-functional teams, which are tightly integrated with our stakeholders for supporting them with insights and analytical products. Currently, we focus on TV & Media and Networks but we continuously explore new areas. Your contribution will be central and you will have a great opportunity to influence the work that your team is doing.

Who are you?

You are independent with a strong drive, yet you are a team player that appreciates an inclusive environment. You are a proactive person who takes initiatives to find solutions to the problems. You have a proven track-record in advanced analytics where you have used your unique blend of analytical, technical and business skills to create value. You like to be challenged in new areas and appreciate the opportunity to get involved in end-to-end product development. It is great if you understand our industry, how we work and our products.

Your personality:

Strong analytical mindset
Business-minded
Excellent communication skills.
Team player
Forward thinking.

Your experience:

Proven experience as a Data Scientist or in a similar role for around 2-3 years
Master degree in Computer Science, Data Science, Mathematics or similar field
Strong skills in statistical analysis and modeling.
Solid understanding of machine learning when especially applied on real world problems in a commercial environment.
Proven experience from advanced analytics environments, with Python as your main language(s). Experience in SQL and Scala is a plus, but not mandatory.
Experience implementing efficient algorithms on the Hadoop ecosystem at scale, i.e. Spark, Hive/Impala.
Hands on experience in deep learning is a bonus, especially if used in business relevant projects.
Experience from Telco and/or TV & Media domain is a bonus but not necessary.
Fluent in English, spoken and written.
Interested?

If you fancy joining my team, apply for the job! Do you need to know more, contact me via email Camilla.sternberg@teliacompany.com or connect with me on LinkedIn. Selection is ongoing, so do not hesitate to get in touch. Las tay to apply is 2020-10-14.

Welcome to Telia – Home to your next big opportunity!