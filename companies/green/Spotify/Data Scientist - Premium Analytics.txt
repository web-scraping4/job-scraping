Do you want to help guide the core business of Spotify using insights from analyses and data?

The mission of our Premium Analytics Team is to bring Spotify Premium to more users across the globe and to constantly evolve and improve our offering. You'll be an essential partner in guiding decisions for shaping the Premium of the future. At your fingertips you’ll have access to petabytes of data, and will get the opportunity to be creative with how you drive insights from that. Above all, your work will impact the way the world experiences music.

You will partner up closely with a global team of world-class analysts, data scientists, business managers and engineers. We are all passionate about what we do, and move forward with high impact projects at a high pace. Learning and improving is part of our daily routine, and you will be free to develop your own skills and ways of working.
What you'll do
Drive exploratory analyses to address large, open-ended questions and find new insights that are key to Spotify’s strategy
Enable Spotify to make the best possible decisions by informing them with relevant learnings
Partner with business managers to explore, discover and discuss new opportunities
Who you are
You are curious, open-minded and enjoy team work in a fast changing environment
You are interested or have experience with business strategy and exploratory analyses
You have a few years of hands-on experience synthesizing insights from data using tools such as Python, R, SQL, Excel, SAS, SPSS, Minitab and/or Hadoop
You have studied Econometrics, Macroeconomics, Statistics, Engineering or any other relevant field
You are welcome at Spotify for who you are, no matter where you come from, what you look like, or what’s playing in your headphones. Our platform is for everyone, and so is our workplace. The more voices we have represented and amplified in our business, the more we will all thrive, contribute, and be brilliant. So bring us your personal experience, your perspectives, and your background. It's in our differences that we will find the power to keep revolutionizing the way the world listens.
Spotify transformed music listening forever when we launched in 2008. Our mission is to unlock the potential of human creativity by giving a million creative artists the opportunity to live off their art and billions of fans the opportunity to enjoy and be inspired by these creators. Everything we do is driven by our love for music and podcasting. Today, we are the world’s most popular audio streaming subscription service with a community of more than 299 million users.