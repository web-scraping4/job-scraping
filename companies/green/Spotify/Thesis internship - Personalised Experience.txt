We are currently looking for a thesis intern to join our Personalisation Product Insights team in Stockholm. We’re working with the Search and Programming Platform product areas in Stockholm and London. We are made up of a combination of user researchers and data scientists. We conduct large scale research mixed methods research projects into topics such as music discovery and podcast promotions, and support product squads with smaller and more tactical work, such as experimentation and usability testing. We are highly collaborative, working across fields, time zones and missions/product areas. As an intern in our team you’d be writing your thesis around the development of personalised user experiences. This thesis would be conducted by applying a qualitative research method.
What you’ll do
Write a thesis project that might include how humans and algorithms come together to produce content, how content is distributed to individuals on a personalised basis, or how it is experienced and consumed in everyday life.
applying qualitative research methods to research problems, by using small qualitative data set to craft the use of Big Data
Who you are
Currently studying for a master degree at a higher education based in Sweden and plan to write your thesis with start in January 2021
Pursuing a Master’s degree within social science, computer science or similar fields of study
You have experience or training in quantitative research methods such as interviews, participant observation, diary studies, qualitative survey design.
You’re planning to use empirical research for your thesis and have an interest in researching how digital tech is shaping people's everyday lives.
The Thesis experience:

This is your chance to deep dive into one of our exciting thesis scopes and joins our 2021 Spring thesis internship program. You’ll be supervised by one of our experienced employees and work with groundbreaking technology. We’re looking for enthusiastic students with a passion for audio experiences and an ambition to go far. This isn't just any Thesis program, you’ll conduct your thesis project around real challenges in our teams and grow your skills within the area of your expertise.

Moreover, you’ll be a part of an interactive group of thesis interns to get to know and share experience and knowledge throughout the program. All over one unforgettable spring!

Sounds like the thesis internship for you? Tell us how you would approach this thesis challenge, why it interests you and what knowledge you think you could contribute with in the research.

Our paid Thesis projects last for 20 weeks and start in the middle of January 2021. The last day to apply is October 18th, 2020 at 11:59 PM, EST. You can expect the following application timeline:

Mid October 2020 - Late October 2020: Application Review
Early November 2020 - Mid November 2020: Interviews
Mid-November 2020: Offers given out
January 2021: Thesis program start

You are welcome at Spotify for who you are, no matter where you come from, what you look like, or what’s playing in your headphones. Our platform is for everyone, and so is our workplace. The more voices we have represented and amplified in our business, the more we will all thrive, contribute, and be brilliant. So bring us your personal experience, your perspectives, and your background. It's in our differences that we will find the power to keep revolutionizing the way the world listens.
Spotify transformed music listening forever when we launched in 2008. Our mission is to unlock the potential of human creativity by giving a million creative artists the opportunity to live off their art and billions of fans the opportunity to enjoy and be inspired by these creators. Everything we do is driven by our love for music and podcasting. Today, we are the world’s most popular audio streaming subscription service with a community of more than 299 million users.