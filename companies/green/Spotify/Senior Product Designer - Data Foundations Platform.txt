The Platform team creates the technology that enables Spotify to learn quickly and scale easily, enabling rapid growth in our users and our business around the globe. Spanning many disciplines, we work to make the business work; creating the frameworks, capabilities and tools needed to welcome a billion customers. Join us and help to amplify productivity, quality and innovation across Spotify.

In Data Foundation we build the tools and technology that powers all data-related work within Spotify, including personalization features such as Discover Weekly, and the business critical royalty calculations. Our platform is used every day by engineers, scientists, and analysts from all parts of the company, and our work has a direct and immediate impact on how fast Spotify can move.

We are looking for a Senior Product Designer, with solid facilitation, research, visual and service design skills, to work with our Data Foundations team in our Platform mission. You’ll be part of a cross functional team that enables Spotify to efficiently produce, manage and consume high quality data by providing an intuitive and enjoyable platform.

You will have a key role to make working with data within Spotify effortless, efficient, and enjoyable, by interacting directly with our users.
What you'll do
Lead design projects for the Data, Machine Learning, and Insights platforms, focusing on providing a cohesive user experience
Understand user and business needs and how to prioritize design work based on insights gained from qualitative and quantitative research
Identify what is needed for each design phase
Partner with Product Managers and Engineers to generate ideas and viable solutions
Help identify and prioritize impactful features through research and workshops
Partner with Product Managers and Engineers to develop viable concepts
Work directly with the engineering teams to implement new features and iterations and ensure quality standards are met
Partner with teams across Spotify to contribute to our broader tool suite
Contribute to creating and maintaining our design system, participate in team critiques and reviews, assist in user research, organize customer events and design workshops
Who you are
You have a passion for complex problems and tools. We do not work on Spotify’s public-facing apps or marketing; we build internal tools for other Spotifiers. It’s crucial that you are excited by this. The rewards come from getting to solve real problems for your colleagues and from having the liberty to move fast and innovate. On our team, designers also have the opportunity to work across multiple teams/products
You have skills/interest in both UX and UI design. You will be doing user research to understand the general problem space, designing overall interaction flows, and also executing fine-grained interface work
You get excited about data. Spotify has amazing data, and we work with it heavily. To make our data useful for other Spotifiers, we need to be able to explore and understand it ourselves. It’s not a hard requirement that you can query or analyze data, but you should have experience working with data-focused applications or data storytelling, and you should have a lot of zeal for diving into this problem space
You are great at collaborating. Working on engineering-focused teams, usually as the only designer, means you have to be a great collaborator and communicator.
You are able to understand engineering goals and problems, and to persuade engineers of the goals and processes of design. You should be comfortable with working within an agile structure that is usually more geared toward engineering.
You are welcome at Spotify for who you are, no matter where you come from, what you look like, or what’s playing in your headphones. Our platform is for everyone, and so is our workplace. The more voices we have represented and amplified in our business, the more we will all thrive, contribute, and be brilliant. So bring us your personal experience, your perspectives, and your background. It's in our differences that we will find the power to keep revolutionizing the way the world listens.
Spotify transformed music listening forever when we launched in 2008. Our mission is to unlock the potential of human creativity by giving a million creative artists the opportunity to live off their art and billions of fans the opportunity to enjoy and be inspired by these creators. Everything we do is driven by our love for music and podcasting. Today, we are the world’s most popular audio streaming subscription service with a community of more than 299 million users.