Our client seek to find scalable solutions that allow us to reach a product-market fit for all our growth markets and audience segments. At our client you will be given the opportunity to be a part of a fast-paced international organization. This organization is continuously moving, and we want you to join the ride!Om tjänstenOBS.This is an open application. We’re the Markets Business Unit, a truly cross-functional team focused on finding future listeners, wherever and whoever they may be. We work collaboratively, combining marketing, insights, product development, design and engineering expertise to grow monthly active users in both new and existing markets across the globe.The team built them by understanding the world of music and podcasts better than anyone else. Join us and you’ll keep millions of users listening by making great recommendations to each and every one of them.Our client is proud to foster a workplace free from discrimination. They strongly believe that diversity of experience, perspectives, and background will lead to a better environment for all employees, and a better product for the users and creators.
This is a consultant assignment. 
Academic Work will be your employer and you will work as a consultant at our client’s company. The assignment will be full time and extend for six months. There are good opportunities to be extended on the assignment, given that all parties are satisfied with the cooperation.
Arbetsuppgifter
Our client are currently working on enabling the localisation of the onboarding experiences and recommendations so that we meet local user expectations and we are starting similar efforts to optimise the Search and Home experiences
Co-operate with cross-functional teams of data scientists, user researchers, product managers, designers and engineers who are passionate about our consumer experience across platforms and partners
Perform analyses on large sets of data to extract impactful insights on user behavior that will help drive product and design decisions
Communicate insights and recommendations to external partners
Be a key partner in our work to build out our ubiquity strategy so that we are relevant in the daily lives of consumers.
Be part of building the next generation experience outside of our native applications.
Work closely with external partners and be creative to understand how we can build, run experimentation and learn from new platforms and product experiences.
Vi söker dig som
CV in English3-8 years of relevant experience, with a degree in statistics, mathematics, computer science, engineering, economics or another quantitative subject
Strong interpersonal skills and are comfortable working with multiple stakeholdersKnow how to understand and tackle loosely defined problems and come up with relevant answers and impactful insights
Coding skills (for example Python, Java or Scala)
Analytics tools experience (such as Pandas, R, SPSS, SQL, or Hadoop)
Statistical competence (such as regression modeling, A/B testing, statistical signifcance, causal inference methods, etc)
You understand the value of collaboration within teams
Other information
Start: Asap
Work extent: Full time, long term
Location: Central Stockholm
This recruitment process is conducted by Academic Work. The request from our client is that all questions regarding the position is handled by Academic WorkApply by pressing the “Apply here” button below. Our selection process is continuous and the advert may close before the recruitment process is completed if we have moved forward to the screening or interview phase.
